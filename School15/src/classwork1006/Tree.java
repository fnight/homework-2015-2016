package classwork1006;

public class Tree {
	
	
	public Tree left;
	public Tree right;
	public int id;
	public Op op;
	
	
	public Tree(Op o, Tree x){ left = x; right = null; id = -1; op = o; }
	public Tree(Op o, Tree l, Tree r){ left = l; right = r; id = -1; op = o; }	
	public Tree(Op o, int i){ left = right = null; id = i; op = o; }
	public Tree(int i){ left = right = null; id = i; op = Op.VAR; }

	public String toString(){
		switch(op){
			case AND: case OR: case IMP:
				return "(" + left.toString() + ")" + op.toString() + "(" + right.toString() + ")";
			case NOT:
				return  op.toString() + "(" + left.toString() + ")";
			case VAR:
				return "A" + Integer.toString(id);
				
		}
		return "???";
	}
	


}
