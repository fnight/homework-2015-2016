package classwork1006;

public enum Op {
	AND,
	OR,
	NOT,
	VAR,
	IMP;
	
	public String toString(){
		switch(this){
			case AND: return "&";
			case OR: return "v";
			case NOT: return "~";
			case VAR: return "VAR";
			case IMP: return "=>";
		}
		
		return "???";
	}
}
