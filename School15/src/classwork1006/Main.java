package classwork1006;



public class Main {

	public static void main(String[] args) {	
		
		Tree tree = new Tree( Op.AND, new Tree(0), new Tree(Op.NOT, new Tree(Op.OR, new Tree(1), new Tree(2))) ); 
//		new Tree(Op.IMP, new Tree(Op.NOT , new Tree(Op.AND, new Tree(1), new Tree(2))), new Tree(Op.OR, new Tree(1), new Tree(2)));
//		new Tree(Op.OR,new Tree(Op.NOT,new Tree(2)), new Tree(Op.IMP, new Tree(3), new Tree(100) ) );
		
		System.out.println(tree.toString());
		
		System.out.println(max_number(tree));
		boolean[] ar  =  { true, false , false };
		System.out.println(meme(tree,ar));
		all(tree);
	}

	
	public static int max_number(Tree tree){
		if (tree.right == null){
			if (tree.id == -1){
				return _max_number(0, tree.left);
			}else{
				return tree.id;
			}
			
		}else{
			return Math.max(_max_number(0, tree.left), _max_number(0, tree.right));
		}
	}
	
	private static int _max_number(int max, Tree tree){
		if (tree.right == null){
			if (tree.id == -1){
				return Math.max(max,_max_number(max, tree.left));
			}else{
				return tree.id;
			}
		}else{
			return Math.max(max, Math.max(_max_number(max, tree.left), _max_number(max, tree.right)));

		}
	}
	
	
	public static boolean isNum(Tree tree){
		if (tree.id != -1 ){
			return true;
		}else{
			return false;
		}
	}
	
	
	public static boolean meme(Tree tree, boolean[] ar){
		switch (tree.op){
		case AND:
			return meme(tree.left,ar)&&meme(tree.right,ar);
		case IMP:
			return (!meme(tree.left,ar))||meme(tree.right,ar);
		case NOT:
			return !meme(tree.left,ar);
		case OR:
			return meme(tree.left,ar)||meme(tree.right,ar);
		case VAR:
			return ar[tree.id];
		}
		return false;
	}
	
	public static boolean[] all(Tree tree){
		int mx = max_number(tree) + 1;
		int a1 = (int) Math.pow(2.0, (double) mx);
		boolean[] ar = new boolean[a1];
		
		for(int i = 0; i<a1; i++){
			String b_ar = Integer.toBinaryString(i);
			boolean[] bool_ar = new boolean[mx];
			for(int j = 0; j<mx; j++){
				bool_ar[j] = false;
			}
			for(int j = mx-1; j>=0; j-- ){
				try{
					if (b_ar.charAt(mx-1-j) == '0'){
						bool_ar[j] = false;
					}else{
						bool_ar[j] = true;
					}
				}catch(StringIndexOutOfBoundsException e){
					bool_ar[j] = false;
				}
			}
			System.out.print("������ "+ i + ": ");
			for(int k = 0; k<mx; k++){
				System.out.print(bool_ar[k] + " ");
			}
			System.out.println();
			ar[i] = meme(tree, bool_ar);
			System.out.println("���������: " + ar[i]);
			
		}
		
		return ar;
	}
	
	
//	public static byte[] count(Tree ar){
//		byte[] ar = new byte[];
//		return ar;
//	}
}
