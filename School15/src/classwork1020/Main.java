package classwork1020;


import java.util.Scanner;

/**
 * Created by Пользователь on 20.10.2015.
 */
public class Main {

    static int num;
    static int aim;
    static int a;
    static int b;
    static int fun1(){return num+=a;}
    static int fun2(){return num*=b;}
    static int fun1(int x){return x+a;}
    static int fun2(int x){return x*b;}


    public static void main(String[] args){

        num =1;
        aim = 22;
        a = 1;
        b = 2;

        interactive();
        //fc();
       // System.out.print(fstp());
        //System.out.println(fstp(num));
        //fc();
        //interactive();

    }


    static void fc (){
        for(int i = 1; i< aim; i++){
            boolean flg = fstp(i);
            if(flg){
                System.out.println("Если в начале было " + i + " камней. Выиграл игрок 1 за " + fstp(i,0)  +   " ходов ");
            }else{
                System.out.println("Если в начале было " + i + " камней. Выиграл игрок 2 за " + fstp(i,0)  +   " ходов ");
            }
        }
    }

    static int fstp ( int n, int count ){
        if(aim <= n) return count;
        else {
            count++;

            if (sndp(fun2(n)) && sndp(fun1(n))) return ( Math.min(sndp(fun1(n),count), sndp(fun2(n),count) ));
            else{
                if(sndp(fun2(n))) return sndp(fun2(n), count);
                else{
                    if(sndp(fun1(n))){
                        return sndp(fun1(n),count);
                    }else{
                        return Math.max(sndp(fun1(n),count), sndp(fun2(n),count) );
                    }
                }
            }
        }
    }

    static int sndp ( int n, int count){
        if(aim <= n){ return count; }
        else{
            count++;
            if (!fstp(fun2(n)) && !fstp(fun1(n))){return ( Math.min(fstp(fun1(n),count), fstp(fun2(n),count) ));}
            else{
                if(!fstp(fun2(n))){
                    return fstp(fun2(n), count);
                }
                else{
                    if(!fstp(fun1(n))){
                        return fstp(fun1(n),count);
                    }else{
                        return Math.max(fstp(fun1(n),count), fstp(fun2(n),count) );
                    }
                }
            }
        }
    }

    static boolean fstp ( int n ){
        if(aim <= n) return false;  else return ( sndp(fun1(n)) || sndp(fun2(n)) );
    }

    static boolean sndp ( int n){
        if(aim <= n) return true; else return ( fstp(fun1(n)) && fstp(fun2(n)) );
    }

    static int sndp_interactive ( int n){
        if(aim <= n){ return n; }
        else{
            if (!fstp(fun2(n))) { return fun2(n); }
            else { return fun1(n);}
        }
    }

    static void interactive (){
        int n = num;
        boolean endgame = false;


        while(!endgame) {
            if (aim <= n) { endgame = true; System.out.print("Вы проиграли"); break;}
            System.out.println("Количество камней : " + n +". Цель :" + aim  +  ". Какую функцию вы хотите выбрать?");
            Scanner sc = new Scanner(System.in);
            int choice = sc.nextInt();
            if (choice == 1) {
                n = fun1(n);
                if (aim <= n) { endgame = true; System.out.print("Вы выиграли"); break;}
                n = sndp_interactive(n);
            } else {
                n = fun2(n);
                if (aim <= n) { endgame = true; System.out.print("Вы выиграли"); break;}
                n = sndp_interactive(n);
            }
        }
    }

}

