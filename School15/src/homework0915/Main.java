package homework0915;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Main {

	public static boolean is_polyndrom(String str){
		//�������� ������ �� ���������
		boolean flag = false;
		
		if (str.length() % 2 == 0){
			if ( str.substring(0,str.length()/2).equals(new StringBuilder(str.substring(str.length()/2)).reverse().toString())){
				flag = true;
			}
		}else{
			if ( str.substring(0,str.length()/2).equals(new StringBuilder(str.substring(str.length()/2+1)).reverse().toString())){
				flag = true;
			}
		}
		return flag;
	}
	
	public static void alphabet(int linelen,int linenum, String alphabet){
		//������ �� �������
		int count =0;
		for (int i=0; i<linenum; i++){
			
//			for (int j = count%(alphabet.length()-1);j!=(count+linelen)%(alphabet.length()-1);  j=(j+1)%(alphabet.length()-1)){
//				
//				System.out.print(alphabet.charAt(j));
//			}
			for (int j = count;j!=(count+linelen);  j++){
				
				System.out.print(alphabet.charAt(j%(alphabet.length()-1)));
			}
			count++;
			System.out.println();
			
		}
	}
	
	public static ArrayList<String> passwords(){
		//������ �� 100 �������� �������
		Random random = new Random();
		String gl = "aeouy";
		String sogl = "bcdfghjklmnpqrstvwxz";
		ArrayList<String> ar = new ArrayList<String>();

		for (int i=0; i<100; i++){
			String str = "";
			for (int j =0; j<10; j++){
				if(j%2==0){
					str = str + (new Character(gl.charAt(random.nextInt(gl.length()))).toString());
				}else{
					str = str + (new Character(sogl.charAt(random.nextInt(sogl.length()))).toString());
				}
			}
			ar.add(str);
		}
		
		return ar;
	}
	
	public static ArrayList<String> sorted_passwords(){
		// ������ �� 100 �������� ��������������� �������
		ArrayList<String> ar =passwords();
		Collections.sort(ar);
		return ar;
	}
	
	public static int[][] square(int n){
		//������� n*n �� ����� �������, ������ - ����
		int[][] ar = new int[n][n];
		for(int i=0; i<n; i++){
			for(int j=0; j<n; j++){
				if(i==0||j==0||i==n-1||j==n-1){
					ar[i][j]=1;
				}else{
					ar[i][j]=0;
				}
			}
		}
		
		
		return ar;
	}
	
	public static void main(String[] args) {
		System.out.println(is_polyndrom("aababaa"));
		alphabet(17, 10, "abcdefg");
		ArrayList<String> ar = sorted_passwords();
		for (int i=0; i<10; i++){
			System.out.println(ar.get(i));
		}
		int[][] arr = square(10);
 		for(int i =0; i<10; i++){
			for(int j : arr[i]){
				System.out.print(j + " ");
			}
			System.out.println();
		}
	}

}
