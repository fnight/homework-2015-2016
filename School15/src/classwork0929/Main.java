package classwork0929;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class Main {

	public static boolean[][] graph_init(String filename){
		FileReader s_in = null;
		try {
			s_in = new FileReader(filename);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		Scanner s = new Scanner( s_in);
        int v = s.nextInt();
        boolean[][] ar = new boolean[v][v];
        
        for (int i=0; i<v; i++){
        	for (int j=0; j<v; j++){
        		ar[i][j] = false;
        	}
        }
        int v1;
        int v2;
        while((v1=s.nextInt())!=-1){
        	v2 = s.nextInt();
        	ar[v1][v2] = true;
        }
        s.close();
        return ar;
	}
	
	public static boolean is_able (boolean[][] ar){
		int[] color = new int[ar.length];
		Arrays.fill(color, -1);
		return _is_able(0,ar,color,0);
	}
	
	private static boolean _is_able (int v1, boolean[][] ar, int[] color, int count){
		
		if(color[v1] == -1){
			color[v1] = count % 2;
			System.out.println(v1 + " " + color[v1]);
			
		}else{
			if (color[v1] != count % 2){
				return false;
			}
		}
		
		count++;
		
		for(int i=0; i<ar.length; i++){
			if(ar[v1][i]){
				if (color[i] == count%2){
					return _is_able (i, ar, color, count);
				}else{
					return false;
				}
			}
		}
		
		
		return true;
	}
	
	

	public static boolean is_able_btw(boolean[][] ar){
		Queue<Integer> q = new LinkedList<Integer>();
		Integer[] color = new Integer[ar.length];
		boolean[] washere = new boolean[ar.length];
		for(int i=0; i< ar.length; i++){washere[i]=false;}
		Arrays.fill(color, -1);
		q.add(0);
		q.add(0);
		int a;
		int b;
		while(Arrays.asList(color).contains(-1)){
			if(!q.isEmpty()){
				a = q.remove();
				b = q.remove();
				washere[a] = true;
				System.out.println(a);
				if((color[a]!=b%2)&&(color[a]!=-1)){ return false; }
			
				color[a] = b % 2; 
				b++;                 
				for(int i=0; i<ar.length; i++){
					if((ar[a][i])){
						if (color[i] == (b+1)%2){
							return false;
						}else{
							if(color[i]==-1){ q.add(i); q.add(b);}
						}
					}
				}
			}else{
				int i =0 ;
				while(i<ar.length){
					if (!washere[i]){
						q.add(i); q.add(0);break;
					}
					i++;
				}
			}
		}
		return true;
	}
	
	public static void test01 () throws IOException{
		PrintWriter writer = new PrintWriter("src/classwork0929/input.in");
		writer.println(99);
		for (int i = 1; i< 98; i++){
			writer.print(0 + " ");
			writer.println(i);
		}
		for (int i = 1; i< 98; i++){
			writer.print(i + " ");
			writer.println(98);
		}
		writer.print(-1);
		writer.close();
	}
	
	public static void test02 () throws IOException{
		PrintWriter writer = new PrintWriter("src/classwork0929/input.in");
		writer.println(99);
		writer.print(-1);
		writer.close();
		
	}
	
	public static void test03 () throws IOException{
		PrintWriter writer = new PrintWriter("src/classwork0929/input.in");
		writer.println(99);
		for (int i = 0; i< 99; i++){
			for (int j = 0; j < 99 ; j++){
				if (i!=j){
					writer.print(i + " ");
					writer.println(j);	
				}
			}
		}
		writer.print(-1);
		writer.close();
	}
	

	
	public static void main(String[] args) throws IOException {
		
		test03();
		
		boolean[][] mtrx = graph_init("src/classwork0929/input.in");
		
		PrintWriter writer = new PrintWriter("src/classwork0929/output.out");
		writer.print(is_able_btw(mtrx));
		writer.close();
	}

}













