package homework0922;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

import classwork0922.Graph;

public class Main {

	public static void mtrx_to_file(String input, String output) throws FileNotFoundException{
		FileReader s_in;
		s_in = new FileReader(input);
		
		Scanner s = new Scanner( s_in);
        int v = s.nextInt();
        boolean[][] matrix = new boolean[v][v];
        int e = 0;
        for (int i=0; i<v; i++){
        	for (int j=0; j<v; j++){
        		int elem =  s.nextInt();
        		if(elem==1){
        			matrix[i][j] = true;
        			e++;
        		}else{
        			matrix[i][j] = false;
        		}
        	}
        }
        s.close();
        
        PrintWriter writer = new PrintWriter(output);
        writer.println(v+" "+e);
        for(int i=0; i<v; i++){
        	for(int j=0; j<v; j++){
        		if (matrix[i][j]){
        			writer.println(i+" "+j);
        		}
        	}
        }
        writer.close();
	}
	
	public static int shortest_way(boolean[][] ar){
		
		Queue<Integer> q = new LinkedList<Integer>();
		int[] dist = new int[ar.length];
		for(int i =0; i< ar.length;i++){
			dist[i]= -1;
		}
		dist[0]=0;
		q.add(0);
		int a;
		int dest = ar[0].length-1;
		while((a = q.remove() ) != dest){
			System.out.println(a);
			int d = dist[a]+1;
			for(int i=0; i<=dest; i++){
				if((ar[a][i])&&dist[i]==-1){
					dist[i]=d;
					q.add(i);
				}
			}
		}
		return dist[dest];
	}
	
	public static int nearest_fork(boolean[][] ar){
		int min = ar.length;
		int mini = -1;
		
		for (int i = 0; i< ar.length; i++){
			if(ar[0][i]){
				int[] helpar = _nearest_fork(i, ar) ;
				if(min >= helpar[0]){
					min = helpar[0];
					mini = helpar[1];
				};
			}
		}
		return mini;
	}
	
	private static int[] _nearest_fork(int v1, boolean[][] ar){
		int[] res = {0,v1};
		for (int i = 0; i< ar.length; i++){
			if(ar[v1][i]){
				res[0]++;
			}
		}
		if (res[0]>1){
			return res;
		}else{
			for (int i = 0; i< ar.length; i++){
				if(ar[v1][i]){
					return _nearest_fork(i,ar);
				}
			}
		}
		return new int[] {ar.length,-1};
	}
	
	public static boolean[][] graph_init(String filename){
		FileReader s_in = null;
		try {
			s_in = new FileReader(filename);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		Scanner s = new Scanner( s_in);
        int v = s.nextInt();
        int e = s.nextInt();
        boolean[][] ar = new boolean[v][v];
        for (int i=0; i<v; i++){
        	for (int j=0; j<v; j++){
        		ar[i][j] = false;
        	}
        }
        for (int i=0; i<e; i++){
        	int v1 = s.nextInt();
        	int v2 = s.nextInt();
        	ar[v1][v2] = true;
        }
        s.close();
        return ar;
	}

	
	public static void main(String[] args) throws FileNotFoundException {
		mtrx_to_file("src/input.txt", "src/output.txt");
		boolean[][] mtrx = graph_init("src/output.txt");
		System.out.println(nearest_fork(mtrx));
		Graph graph = new Graph("src/output.txt");
		ArrayList<ArrayList<Integer>> all_ways =  graph.all_ways();
		
		System.out.println("��� ���� �� 0 � " + (graph.getAr().length -1));
		for(int i = 0; i<all_ways.size(); i++){
			for (int j = 0; j< all_ways.get(i).size(); j++){
				System.out.print(all_ways.get(i).get(j) + " ");
			}
			System.out.println();
		}
	}

}
