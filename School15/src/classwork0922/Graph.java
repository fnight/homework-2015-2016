package classwork0922;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
import java.util.ArrayList;


public class Graph {

	
	private boolean[][] ar;
	
	public Graph(int n) {
		ar = new boolean[n][n];
		for(boolean[] i:ar){
			Arrays.fill(i,false);
		}
	}
	
	public Graph(String filename){
		FileReader s_in = null;
		try {
			s_in = new FileReader(filename);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		Scanner s = new Scanner( s_in);
        int v = s.nextInt();
        int e = s.nextInt();
        ar = new boolean[v][v];
        for (int i=0; i<v; i++){
        	for (int j=0; j<v; j++){
        		ar[i][j] = false;
        	}
        }
        for (int i=0; i<e; i++){
        	int v1 = s.nextInt();
        	int v2 = s.nextInt();
        	ar[v1][v2] = true;
        }
        s.close();
	}
	

	
	public boolean[][] graph_init(String filename){
		FileReader s_in = null;
		try {
			s_in = new FileReader(filename);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		Scanner s = new Scanner( s_in);
        int v = s.nextInt();
        int e = s.nextInt();
        ar = new boolean[v][v];
        for (int i=0; i<v; i++){
        	for (int j=0; j<v; j++){
        		ar[i][j] = false;
        	}
        }
        for (int i=0; i<e; i++){
        	int v1 = s.nextInt();
        	int v2 = s.nextInt();
        	ar[v1][v2] = true;
        }
        s.close();
        return ar;
	}
	
	
	public void addEdge(int v1, int v2){
		ar[v1][v2] = true;
	}
	
	public void mtrx_to_file(String input, String output) throws FileNotFoundException{
		FileReader s_in;
		s_in = new FileReader(input);
		
		Scanner s = new Scanner( s_in);
        int v = s.nextInt();
        boolean[][] matrix = new boolean[v][v];
        int e = 0;
        for (int i=0; i<v; i++){
        	for (int j=0; j<v; j++){
        		int elem =  s.nextInt();
        		if(elem==1){
        			matrix[i][j] = true;
        			e++;
        		}else{
        			matrix[i][j] = false;
        		}
        	}
        }
        s.close();
        
        
        
        PrintWriter writer = new PrintWriter(output);
        writer.println(v+" "+e);
        for(int i=0; i<v; i++){
        	for(int j=0; j<v; j++){
        		if (matrix[i][j]){
        			writer.println(i+" "+j);
        		}
        	}
        }
        
        writer.close();
        
	}
	
	public  void printedges(int v){
		
		int count =0;
		for (int i =0; i<ar.length; i++){
			if (ar[i][v]){
				count++;
			}
		}
		System.out.println("� ������� " + v + " ���� " +  count + " �����.");
		
	}
	
	public  boolean is_way(int v1){
		boolean[] helpar = new boolean[ar.length];
		for(int i=0; i<ar.length; i++){
			helpar[i] = false;
		}
		return _is_way(helpar, v1);
	}
	
	private boolean _is_way(boolean[] washere, int v1){
		
		
		if (v1 == ar.length -1){
			return true;
		}
		if (washere[v1] == false ){
			washere[v1] = true;
			for(int i=0; i<ar.length; i++){
				if ((ar[v1][i])&&(_is_way(washere, i))){
					return true;
				}
			}
		}
		return false;
	}
	
	
	public int ways(){
		boolean[] washere = new boolean[ar.length];
		for(int i=0; i<ar.length; i++){
			washere[i] = false;
		}
		
		return _ways(0, washere);
	}
	
	private int _ways(int v1, boolean[] washere){
		
		if ((v1 == ar.length -1)/*&&(!washere[v1])*/){
			System.out.println("����� ����.");
			return 1;
		}else{
			int count = 0;
			washere[v1] = true;
			for (int i=0; i<ar.length; i++){
				if((ar[v1][i])&&(is_way(i))){
					System.out.println("���� � ������� " + i);
					count +=  _ways(i, washere);
				}
			}
			System.out.println("���������� ��������: " + count);
			return count;
		}
	}
	
	public int shortest_way(){
		
		Queue<Integer> q = new LinkedList<Integer>();
		int[] dist = new int[ar.length];
		for(int i =0; i< ar.length;i++){
			dist[i]= -1;
		}
		dist[0]=0;
		q.add(0);
		int a;
		int dest = ar[0].length-1;
		while((a = q.remove() ) != dest){
			System.out.println(a);
			int d = dist[a]+1;
			for(int i=0; i<=dest; i++){
				if((ar[a][i])&&dist[i]==-1){
					dist[i]=d;
					q.add(i);
				}
			}
		}
		return dist[dest];
	}
	
	
	private ArrayList<ArrayList<Integer>> _ways_ar;
	
	public ArrayList<ArrayList<Integer>> all_ways(){
		_ways_ar = new ArrayList<ArrayList<Integer>>();
		Integer[] res = new Integer[ar.length];
		_all_ways(0, 1, res);
		return  _ways_ar;
	}
	
	private void _all_ways(int v1, int count, Integer[] res){
		if (v1 == ar.length -1){
			System.out.println("����� ����.");
			for (int i = 0 ; i<res.length; i++){
				System.out.print(i + " ");
			}
			System.out.println();
			_ways_ar.add(new ArrayList<Integer>(Arrays.asList(res)));
		}else{
			for (int i=0; i<ar.length; i++){
				if((ar[v1][i])&&(is_way(i))){
					System.out.println("���� � ������� " + i);
					res[count] = i;
					count++;
					_all_ways(i, count, res);
				}
			}
		}
		
	}
	
	
	
	
	private long[] dyn;
	
	
	public long ways_dynamic(){
		dyn = new long[ar.length];
		
		for(int i=0; i<ar.length; i++){
			dyn[i] = -1;
		}
		dyn[0] = 1;
		
		int v = ar.length-1;
		
		return _ways_dynamic(v);
	}
	
	
	private long _ways_dynamic(int v){
		System.out.println(v);
		long count = 0;
		for(int i = 0; i<ar.length; i++){
			if(ar[i][v]){	
				if(dyn[i]!=-1){
					count+= dyn[i];
				}else{
					count+= _ways_dynamic(i);
				}
			}
		}
		
		dyn[v] = count;
		return count;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
	}
	
	public int nearest_fork(){
		int min = ar.length;
		int mini = -1;
		
		for (int i = 0; i< ar.length; i++){
			if(ar[0][i]){
				int[] helpar = _nearest_fork(i) ;
				if(min >= helpar[0]){
					min = helpar[0];
					mini = helpar[1];
				};
			}
		}
		return mini;
	}
	
	private int[] _nearest_fork(int v1){
		int[] res = {0,v1};
		for (int i = 0; i< ar.length; i++){
			if(ar[v1][i]){
				res[0]++;
			}
		}
		if (res[0]>1){
			return res;
		}else{
			for (int i = 0; i< ar.length; i++){
				if(ar[v1][i]){
					return _nearest_fork(i);
				}
			}
		}
		return new int[] {ar.length,-1};
	}
	

	public boolean[][] getAr() {
		return ar;
	}

}
