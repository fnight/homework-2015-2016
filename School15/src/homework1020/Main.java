package homework1020;

/**
 * Created by Пользователь on 27.10.2015.
 */
public class Main {

    static int num, aim, a1, b1, a2, b2;
    static int a1(int x){return x+a1;}
    static int b1(int x){return x*b1;}
    static int a2(int x){return x+a2;}
    static int b2(int x){return x*b2;}


    public static void main(String[] args){
        num = 1; aim = 10; a1 = 5; b1 = 2; a2 = 5; b2 = 2;
        System.out.println(from3to10(222000111));
        System.out.println(from3to10(122212221));
        System.out.println(from3to10(021021021));
        System.out.println(new TicTacToe(show(3020)).show());
        printstate(new TicTacToe(122212221).c_state());
        printstate(new TicTacToe(021021021).c_state());
        TicTacToe ticTacToe = new TicTacToe(from3to10(222000222));
//        System.out.println(ticTacToe.show());
//        ticTacToe.set(2,2,0);
//        System.out.println(ticTacToe.show());
//        System.out.println(ticTacToe.is_win());

//        System.out.println(new TicTacToe((int) Math.pow(3, 9) - 1).get(0, 2));
//        System.out.println((int) (Math.pow(3, 9)) -1);
//        System.out.println(new TicTacToe((int) (Math.pow(3, 9)) -1).show());
    }
    public static int from3to10(int field) {
        int value = field;
        int result = 0;
        int length = (int) Math.log10(value);
        for (int i = 0; i <= length; i++) {
            int a = value % 10;
            value /= 10;
            result += a * (int) Math.pow(3.0, (double) i);
        }
        return result;
    }

    public static void printstate(state s){
        switch (s){
            case WIN: System.out.println("WIN X"); break;
            case DEFEAT: System.out.println("WIN X"); break;
            case DRAW: System.out.println("DRAW"); break;
            case EPS: System.out.println("DRAW"); break;
        }
    }
    public static int show(int field) {
        int value = field;
        String result = "";
        while (value / 3 > 0) {
            int p = value / 3;
            int q = value % 3;
            result = q + result;
            value = p;
        }
        return Integer.parseInt(result);
    }
}

enum state{ WIN, DEFEAT, DRAW, EPS}

class TicTacToe{
    // "0" = O; "1" = X; "2" = " "
    //  2 * 3**8 + 2 * 3**7+ 2 * 3**6 + 2 * 3**5+ 2 * 3**4+ 2 * 3**3+ 2 * 3**2+ 2 * 3**1+ 2 * 3**0
    //    x
    //  y 2 2 2
    //    2 2 2
    //    2 2 2
    private int field;
    public TicTacToe(){
        field = (int) Math.pow(3, 9) - 1;
    }
    public TicTacToe(int f) {
        field = f;
    }
    public int get(int x, int y){
        return (field/ (int) (Math.pow(3,x+y*3)))%3;
//        int base = 8-(x*3+y);
//        int k = (field / (int) Math.pow(10,base) );
//        return k - (int) Math.pow(10, (x*3+y)) ;
    }
    public void set(int x, int y, int s){
        int k = get(x,y);
        int base = (int) Math.pow(3.0, 8- (x + y * 3));
        field = field - k* base + s*base;
    }

    public int show() {
        int value = field;
        String result = "";
        while (value / 3 > 0) {
            int p = value / 3;
            int q = value % 3;
            result = q + result;
            value = p;
        }
        return Integer.parseInt(result);
    }

    public void setField(int i){
        field = i;
    }

    public state is_win(){
        state flag = state.EPS;
        for (int i = 0; i<3; i++){
            if((get(i,0) ==1 && 1 == get(i,1) && 1 == get(i,2)) || (get(0,i) == 1 && 1 == get(1,i) && 1 == get(2,i))) flag = state.WIN;
        }
        if((get(0,0) ==1 && 1 == get(1,1) && 1 == get(2,2)) || (get(0,2) ==1 && 1== get(1,1) && 1 == get(2,0))) flag = state.WIN;
        return flag;
    }

    public state c_state(){
        boolean flag1 = false; // есть ли 3 в ряд крестиков
        for (int i = 0; i<3; i++){
            if((get(i,0) ==1 && 1 == get(i,1) && 1 == get(i,2)) || (get(0,i) == 1 && 1 == get(1,i) && 1 == get(2,i))) flag1 = true;
        }
        if((get(0,0) ==1 && 1 == get(1,1) && 1 == get(2,2)) || (get(0,2) ==1 && 1== get(1,1) && 1 == get(2,0))) flag1 = true;
        boolean flag2 = false; // есть ли 3 в ряд ноликов
        for (int i = 0; i<3; i++){
            if((get(i,0) ==0 && 0 == get(i,1) && 0 == get(i,2)) || (get(0,i) == 0 && 0 == get(1,i) && 0 == get(2,i))) flag2 = true;
        }
        if((get(0,0) ==0 && 0 == get(1,1) && 0 == get(2,2)) || (get(0,2) ==0 && 0== get(1,1) && 0 == get(2,0))) flag2 = true;
        if(flag1 && flag2) return state.DRAW; else if (flag1) return state.WIN; else if (flag2) return state.DEFEAT; else return state.EPS;
    }

    public state is_draw(){
        state flag = state.DRAW;
        for(int i = 0; i<3; i++){
            for(int j=0; j<3; j++){
                if(get(i,j) == 0) flag = state.EPS;
            }
        }
        return flag;
    }


//    public state opt1(){
//        state flag = state.EPS;
//        boolean is_all = false;
//        for (int i = 0; i<3 ; i++){
//            for(int j =0; j<3; j++){
//                set(i,j,0);
//                if(new TicTacToe().opt2() == state.WIN) {flag = state.WIN; is_all = true; break;}
//            }
//            if (is_all) {break;}
//        }
//
//    }
//    public state opt2(){
//
//    }

}
